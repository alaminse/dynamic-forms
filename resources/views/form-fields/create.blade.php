@extends('layouts.app_one')
@section('content')
<div class="container">
    <h1>Create Form Field for {{ $formTemplate->title }}</h1>

    <form method="POST" action="{{ route('form-templates.form-fields.store', $formTemplate) }}">
        @csrf
        <div class="form-group">
            <label for="field_type">Field Type</label>
            <input type="text" class="form-control" id="field_type" name="field_type" required>
        </div>
        <div class="form-group">
            <label for="label">Label</label>
            <input type="text" class="form-control" id="label" name="label" required>
        </div>
        <div class="form-group">
            <label for="options">Options</label>
            <textarea class="form-control" id="options" name="options"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </form>
</div>
@endsection
