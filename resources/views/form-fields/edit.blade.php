@extends('layouts.app_one')

@section('content')
<div class="container">
    <h1>Edit Form Field for {{ $formTemplate->title }}</h1>

    <form method="POST" action="{{ route('form-templates.form-fields.update', [$formTemplate, $formField]) }}">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="field_type">Field Type</label>
            <input type="text" class="form-control" id="field_type" name="field_type" value="{{ $formField->field_type }}" required>
        </div>
        <div class="form-group">
            <label for="label">Label</label>
            <input type="text" class="form-control" id="label" name="label" value="{{ $formField->label }}" required>
        </div>
        <div class="form-group">
            <label for="options">Options</label>
            <textarea class="form-control" id="options" name="options">{{ $formField->options }}</textarea>
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>
</div>
@endsection
