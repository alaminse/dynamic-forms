@extends('layouts.app_one')

@section('content')
<div class="container">
    <h1>Form Fields for {{ $formTemplate->title }}</h1>

    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <a href="{{ route('form-templates.edit', $formTemplate) }}" class="btn btn-primary">Back to Form Template</a>
    <a href="{{ route('form-templates.form-fields.create', $formTemplate) }}" class="btn btn-primary">Create Form Field</a>

    <table class="table mt-3">
        <thead
