@extends('layouts.app_one')

@section('content')
<div class="container">
    <h1>Form Templates</h1>

    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <a href="{{ route('form-templates.create') }}" class="btn btn-primary">Create Form Template</a>

    <table class="table mt-3">
        <thead>
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Category</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse($formTemplates as $formTemplate)
                <tr>
                    <td>{{ $formTemplate->title }}</td>
                    <td>{{ $formTemplate->description }}</td>
                    <td>{{ $formTemplate->category->name }}</td>
                    <td>
                        <a href="{{ route('form-templates.edit', $formTemplate) }}" class="btn btn-primary btn-sm">Edit</a>
                        <form method="POST" action="{{ route('form-templates.destroy', $formTemplate) }}" class="d-inline">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure you want to delete this form template?')">Delete</button>
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">No form templates found.</td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection
