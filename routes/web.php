<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\FormFieldController;
use App\Http\Controllers\FormTemplateController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Example route in routes/web.php

Route::get('/admin/dashboard', function () {
    return view('dashboard');
})->middleware(['auth','role:Administrator', 'verified'])->name('admin.dashboard');
Route::get('/user/dashboard', function () {
    return view('dashboard');
})->middleware(['auth','role:User', 'verified'])->name('user.dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

Route::resource('categories', CategoryController::class);
Route::resource('form-templates', FormTemplateController::class);
Route::resource('form-templates', FormFieldController::class);

