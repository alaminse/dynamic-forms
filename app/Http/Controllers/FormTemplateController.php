<?php
namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\FormTemplate;
use Illuminate\Http\Request;

class FormTemplateController extends Controller
{
    public function index()
    {
        $formTemplates = FormTemplate::all();
        return view('form_templates.index', compact('formTemplates'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('form_templates.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required|exists:categories,id',
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
        ]);

        FormTemplate::create([
            'category_id' => $request->input('category_id'),
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'created_by' => auth()->user()->id,
        ]);

        return redirect()->route('form-templates.index')->with('success', 'Form template created successfully.');
    }

    public function edit(FormTemplate $formTemplate)
    {
        $categories = Category::all();
        return view('form_templates.edit', compact('formTemplate', 'categories'));
    }

    public function update(Request $request, FormTemplate $formTemplate)
    {
        $request->validate([
            'category_id' => 'required|exists:categories,id',
            'title' => 'required|string|max:255',
            'description' => 'nullable|string',
        ]);

        $formTemplate->update([
            'category_id' => $request->input('category_id'),
            'title' => $request->input('title'),
            'description' => $request->input('description'),
        ]);

        return redirect()->route('form-templates.index')->with('success', 'Form template updated successfully.');
    }

    public function destroy(FormTemplate $formTemplate)
    {
        $formTemplate->delete();
        return redirect()->route('form-templates.index')->with('success', 'Form template deleted successfully.');
    }
}
