<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\FormTemplate;
use App\Models\FormField;

class FormFieldController extends Controller
{
    public function create(FormTemplate $formTemplate)
    {
        return view('form-fields.create', compact('formTemplate'));
    }

    public function store(Request $request, FormTemplate $formTemplate)
    {
        $request->validate([
            'field_type' => 'required|string|max:255',
            'label' => 'required|string|max:255',
            'options' => 'nullable|string',
        ]);

        FormField::create([
            'template_id' => $formTemplate->id,
            'field_type' => $request->input('field_type'),
            'label' => $request->input('label'),
            'options' => $request->input('options'),
        ]);

        return redirect()->route('form-templates.edit', $formTemplate)->with('success', 'Form field created successfully.');
    }

    public function edit(FormField $formField)
    {
        return view('form-fields.edit', compact('formField'));
    }

    public function update(Request $request, FormField $formField)
    {
        $request->validate([
            'field_type' => 'required|string|max:255',
            'label' => 'required|string|max:255',
            'options' => 'nullable|string',
        ]);

        $formField->update([
            'field_type' => $request->input('field_type'),
            'label' => $request->input('label'),
            'options' => $request->input('options'),
        ]);

        return redirect()->route('form-templates.edit', $formField->template)->with('success', 'Form field updated successfully.');
    }

    public function destroy(FormField $formField)
    {
        $formField->delete();
        return redirect()->route('form-templates.edit', $formField->template)->with('success', 'Form field deleted successfully.');
    }
}
